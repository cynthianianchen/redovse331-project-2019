import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs';
import { MixdataTableDataSource } from 'src/app/Teacher/mixdata-table/mixdata-table-datasource';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import Mixdata from 'src/app/entity/mixdata';
import Student from 'src/app/entity/student';
import { MixdataService } from 'src/app/service/mixdata.service';

@Component({
  selector: 'app-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.css']
})
export class CheckComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Mixdata>;
  dataSource: MixdataTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  
  displayedColumns = ['id','studentId','lastname','firstname','nickname','gpa','advisor','height','weight','dob','image','description','AcceptBtn','RefuseBtn'];
  mixdata: Mixdata[];
  student: Student[];
 
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private mixdataService : MixdataService) { }

  ngOnInit() {
    this.mixdataService.getMixdatas()
      .subscribe(mixdata => {
        this.dataSource = new MixdataTableDataSource();
        this.dataSource.data = mixdata;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.mixdata = mixdata;
        
      }
      )

  }

  ngAfterViewInit() {}
    
    
    
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }
    
    accept(){
    
      Swal.fire({
        title: 'Accept student registration system ?',
        //text: "You won't be able to revert this!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#c2185b',
        cancelButtonColor: '#000',
        confirmButtonText: 'Accept'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Successful accept',
            '',
            'success'
          )
        }
      })
    
  }

  refuse(){
    
      Swal.fire({
        title: 'Refuse student registration system ?',
        //text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#c2185b',
        cancelButtonColor: '#000',
        confirmButtonText: 'Refuse'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Successful refuse',
            '',
            'success'
          )
        }
      })
    
  }




}