import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 

  constructor() { }

  ngOnInit() {
  }
  add(): void {
    
    Swal.fire({
      title: 'Register ?',
      //text: "You won't be able to revert this!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#c2185b',
      cancelButtonColor: 'black',
      confirmButtonText: 'Yes, Register'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Registered successfully',
           '',
          'success',
        )
        
      }
    })
  }

}
