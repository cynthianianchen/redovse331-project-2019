import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsComponent } from './list/students.component';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from 'src/app/shared/file-not-found/file-not-found.component';
import { StudentsAddComponent } from './add/students.add.component';
import { UpdateComponent } from './update/update.component';
import { RegisterComponent } from './register/register.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { ActivityTableComponent } from './activity-table/activity-table.component';
import { AddComponent } from '../Admin/add/add.component';
import { CheckComponent } from '../Admin/check/check.component';
import { ViewComponent } from '../Teacher/view/view.component';
import { AdminDashboardComponent } from '../Admin/admin-dashboard/admin-dashboard.component';
import { TeachersComponent } from '../Teacher/teacher-dashboard/teachers.component';
import { HomeComponent } from '../home/home.component';
import { MixdataTableComponent } from '../Teacher/mixdata-table/mixdata-table.component';
import { UpdateListComponent } from '../Teacher/update-list/update-list.component';



const StudentRoutes: Routes = [
 {
path:'student-dashboard',
component: StudentDashboardComponent,
children:[


  { path: 'add', component: StudentsAddComponent },
  { path: 'List', component: ActivityTableComponent },
  { path: 'update', component:UpdateComponent},
  { path: 'register', component:RegisterComponent}
]},

 {
  path:'teacher-dashboard',
  component: TeachersComponent,
  children:[
   { path: 'view', component: ViewComponent },
   { path: 'view/mixdatatable', component: MixdataTableComponent},
   { path: 'view/updatelist/:id', component: UpdateListComponent}
  //  { path: 'view/detail/:id' , component: UpdateListComponent},
  ]},

  {
    path:'admin-dashboard',
    component: AdminDashboardComponent,
    children:[
  { path: 'addadmin', component: AddComponent },
  { path: 'check', component: CheckComponent },
   ]},

  { path: 'admin-dashboard', component: AdminDashboardComponent },
  { path: 'student-dashboard', component: StudentDashboardComponent },
  { path: 'teacher-dashboard', component:  TeachersComponent },
  { path: 'home', component: HomeComponent},
  // { path: 'mixdatatable', component: MixdataTableComponent}


  
 
  
//   { path: 'detail/:id', component: StudentsViewComponent}

]


@NgModule({
  imports: [
    RouterModule.forRoot(StudentRoutes)
  ],
  exports: [
    RouterModule
  ] 
})
export class StudentRoutingModule { }
