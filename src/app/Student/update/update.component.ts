import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from '../../service/student-service';
import Student from '../../entity/student';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent {
students: Student[];
  student:Student;

  constructor(private route: ActivatedRoute, private studentService: StudentService){}

  ngOnInit(): void {
    this.route.params
     .subscribe((params: Params) => {
     this.studentService.getStudent(+params['id'])
     .subscribe((inputStudent: Student) => this.student = inputStudent);
     });
    
  }
  update(){
    Swal.fire({
      title: 'Update information ?',
      //text: "You won't be able to revert this!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#c2185b',
      cancelButtonColor: 'black',
      confirmButtonText: 'Yes, Update'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Updated successfully',
           '',
          'success',
        )
      }
    })
  }
}
