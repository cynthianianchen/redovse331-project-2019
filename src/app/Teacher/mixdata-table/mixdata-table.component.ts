import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import Mixdata from 'src/app/entity/mixdata';
import { MixdataService } from 'src/app/service/mixdata.service';
import { MixdataTableDataSource } from './mixdata-table-datasource';
import Student from 'src/app/entity/student';

@Component({
  selector: 'app-mixdata-table',
  templateUrl: './mixdata-table.component.html',
  styleUrls: ['./mixdata-table.component.css']
})
export class MixdataTableComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Mixdata>;
  dataSource: MixdataTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  
  displayedColumns = ['id','studentId','fullname','advisor','image','description','ApproveBtn','RejectBtn'];
  mixdata: Mixdata[];
  student: Student[];
 
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private mixdataService : MixdataService) { }

  ngOnInit() {
    this.mixdataService.getMixdatas()
      .subscribe(mixdata => {
        this.dataSource = new MixdataTableDataSource();
        this.dataSource.data = mixdata;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.mixdata = mixdata;
        
      }
      )

  }

  ngAfterViewInit() {}
    
    
    
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }
  
    confirm() {
      if (confirm("Are you sure to enroll this activity ?")) {
        window.location.pathname = '/enrolledPage'
      }
    }
    
    approve(){
    
      Swal.fire({
        title: 'Approve the student to participate in the activity ?',
        //text: "You won't be able to revert this!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#c2185b',
        cancelButtonColor: '#000',
        confirmButtonText: 'Approve'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Successful approval',
            '',
            'success'
          )
        }
      })
    
  }

  reject(){
    
      Swal.fire({
        title: 'Reject the student to participate in the activity?',
        //text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#c2185b',
        cancelButtonColor: '#000',
        confirmButtonText: 'Reject'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Successful Reject',
            '',
            'success'
          )
        }
      })
    
  }




}
