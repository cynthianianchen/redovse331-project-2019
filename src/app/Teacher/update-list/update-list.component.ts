import { Component, OnInit } from '@angular/core';
import Activity from 'src/app/entity/activity';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ActivityService } from 'src/app/service/activity.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-list',
  templateUrl: './update-list.component.html',
  styleUrls: ['./update-list.component.css']
})
export class UpdateListComponent implements OnInit {

  activitys: Activity[];
  activity: Activity;
  constructor(private route: ActivatedRoute,private activityService: ActivityService,private router: Router) { }

  ngOnInit():void {

    this.route.params
    .subscribe((params: Params) => {
      this.activityService.getActivity(params['id'])
      .subscribe((inputActivity: Activity) => this.activity = inputActivity);
    })
  }

  update(){
    Swal.fire({
      title: 'Continue to update?',
      //text: "You won't be able to revert this!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#c2185b',
      cancelButtonColor: '#000',
      confirmButtonText: 'Update'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Update successful',
          '',
          'success'
        )
      }
    })
  }
}

