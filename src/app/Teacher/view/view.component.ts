import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Activity from 'src/app/entity/activity';
// import { ActivityTableDataSource } from './activity-table-datasource';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity.service';
import Swal from 'sweetalert2';
import { ActivityTableDataSource } from 'src/app/Student/activity-table/activity-table-datasource';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'activityname', 'activitydescription', 'period','Date','location','host','updateBtn','viewBtn'];
  constructor(private activityService: ActivityService) { }
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  ngOnInit() {
    this.dataSource = new ActivityTableDataSource();
    this.activityService.getActivitys()
    .subscribe(activities => {
      this.dataSource = new ActivityTableDataSource();
      this.dataSource.data = activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.activities = this.activities;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
    }
    );
  }

  ngAfterViewInit() {}
    
    
    
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }
  
    confirm() {
      if (confirm("Are you sure to enroll this activity ?")) {
        window.location.pathname = '/enrolledPage'
      }
    }

  }
