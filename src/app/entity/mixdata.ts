export default class Mixdata {
  id: number;
  studentId: string;
  name: string;
  surname: string;
  activityname:string;
  activitydescription:string;
  Date:string;
  image: string;
  location:string;
  advisor:string;
  periodstart:string;
  periodend:string;
  teacher:string;
}
  