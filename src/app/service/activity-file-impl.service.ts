import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ActivityService } from './activity.service';
import Activity from '../entity/activity';


@Injectable({
  providedIn: 'root'
})
export  abstract class ActivityFileImplService extends ActivityService{
  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity[]>('assets/activity.json')
      .pipe(map(activities => {
        const output: Activity = (activities as Activity[]).find(activity => activity.id === +id);
        return output;
      }));

  }
  constructor(private http: HttpClient) {
    super();
  }
  getActivitys(): Observable<Activity[]> {
    return this.http.get<Activity[]>('assets/activity.json');
  }
  saveActivity(student: Activity):Observable<Activity>{
    return null;
  }
}