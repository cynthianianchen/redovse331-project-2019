import { Observable } from '../../../node_modules/rxjs';
import Activity from '../entity/activity';

export abstract class ActivityService {
     abstract getActivitys(): Observable<Activity[]>;
     abstract getActivity(id: number): Observable<Activity>;
}